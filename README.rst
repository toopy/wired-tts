Wired Text To Speech
====================

gTTS based Wired story reader.


Install
-------

Just `pip install` as follow:

.. code:: bash

    $ pip install wired-tts


Usage
-----

Just call it with your article url as follow:

.. code:: bash

    $ wired-tts https://www.wired.com/story/tezos-blockchain-love-story-horror-story/


Hacking the CLI allows you to choose the lang option as follow:

.. code:: bash

    $ wired-tts -l "fr-FR" https://usbeketrica.com/article/facebook-face-au-defi-de-la-moderation-des-contenus


License
-------

MIT License
